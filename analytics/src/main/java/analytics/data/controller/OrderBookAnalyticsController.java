package analytics.data.controller;

import analytics.data.model.OrderBook;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class OrderBookAnalyticsController {

    @PostMapping("/orderBook-analytics")
    public ResponseEntity<String> getOrderBooks(@RequestBody List<OrderBook> orderBooks) {
        System.out.println(orderBooks.size());
        return ResponseEntity.ok("OrderBook alerts send successfully.");
    }
}
