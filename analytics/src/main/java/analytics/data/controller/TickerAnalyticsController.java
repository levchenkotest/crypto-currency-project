package analytics.data.controller;

import analytics.data.model.OrderBook;
import analytics.data.model.Ticker;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class TickerAnalyticsController {

    @PostMapping("/price-analytics")
    public ResponseEntity<String> getOrderBooks(@RequestBody List<Ticker> tickers) {
        System.out.println(tickers.size());
        return ResponseEntity.ok("Price alerts send successfully.");
    }
}
