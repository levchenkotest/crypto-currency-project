package market.data.service;

import com.binance.connector.client.impl.SpotClientImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import market.data.client.HistoricalDataRestTemplateClient;
import market.data.model.Symbols;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@AllArgsConstructor
@Slf4j
@EnableScheduling
public class MarketDataService {

    private final SpotClientImpl binanceSpotClientImpl;
    private final HistoricalDataRestTemplateClient historicalDataRestTemplateClient;

    @Scheduled(fixedRate = 60000)
    public ResponseEntity<String> fetchLatestPrices() {
        Map<String, Object> tickers = new HashMap<>();
        tickers.put("symbols", Symbols.getAllSymbols());
        String result = binanceSpotClientImpl.createMarket().ticker(tickers);

        ResponseEntity<String> response = historicalDataRestTemplateClient.postTickerData(result);

        if (response.getStatusCode().is2xxSuccessful()) {
            return ResponseEntity.ok("Historical price stored successfully.");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to store historical price.");
        }
    }

    @Scheduled(fixedRate = 60000*30)
    public ResponseEntity<String> fetchOrderBook() {
        Map<String, Object> tickers = new HashMap<>();
        tickers.put("symbols", Symbols.getAllSymbols());
        String result = binanceSpotClientImpl.createMarket().bookTicker(tickers);

        ResponseEntity<String> response = historicalDataRestTemplateClient.postOrderBookData(result);

        if (response.getStatusCode().is2xxSuccessful()) {
            return ResponseEntity.ok("Historical orderBooks stored successfully.");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to store historical orderBooks.");
        }
    }
}
