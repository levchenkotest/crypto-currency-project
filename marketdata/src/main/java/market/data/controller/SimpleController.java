package market.data.controller;

import jakarta.annotation.security.RolesAllowed;
import lombok.AllArgsConstructor;
import market.data.utils.UserContextHolder;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class SimpleController {

    @RolesAllowed({ "ADMIN", "USER" })
    @GetMapping("/hello")
    public ResponseEntity<String> hell() {
        String correlationId = UserContextHolder.getContext().getCorrelationId();
        return ResponseEntity.ok("Hello, correlationId: " + correlationId);
    }
}
