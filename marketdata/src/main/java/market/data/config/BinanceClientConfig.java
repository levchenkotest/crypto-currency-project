package market.data.config;

import com.binance.connector.client.impl.SpotClientImpl;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class BinanceClientConfig {

    @Bean
    public SpotClientImpl binanceSpotClientImpl() {
        return new SpotClientImpl();
    }
}
