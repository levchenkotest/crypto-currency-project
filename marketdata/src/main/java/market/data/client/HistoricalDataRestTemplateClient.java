package market.data.client;

import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.bulkhead.annotation.Bulkhead.Type;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import market.data.utils.UserContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class HistoricalDataRestTemplateClient {

    private static final Logger logger = LoggerFactory.getLogger(HistoricalDataRestTemplateClient.class);

    private final RestTemplate restTemplate;

    public HistoricalDataRestTemplateClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @CircuitBreaker(name = "historicalDataService", fallbackMethod = "buildFallbackMethod")
    @RateLimiter(name = "historicalDataService", fallbackMethod = "buildFallbackMethod")
    @Retry(name = "retryHistoricalDataService", fallbackMethod = "buildFallbackMethod")
    @Bulkhead(name = "bulkheadHistoricalDataService", type= Type.THREADPOOL, fallbackMethod = "buildFallbackMethod")
    public ResponseEntity<String> postTickerData(String data) {
        String historicalDataServiceUrl = "http://historicaldata/tickers";
        logger.debug("HistoricalDataRestTemplateClient Correlation id: {}",
                UserContextHolder.getContext().getCorrelationId());
        return restTemplate.postForEntity(historicalDataServiceUrl, data, String.class);
    }

    @CircuitBreaker(name = "historicalDataService", fallbackMethod = "buildFallbackMethod")
    @RateLimiter(name = "historicalDataService", fallbackMethod = "buildFallbackMethod")
    @Retry(name = "retryHistoricalDataService", fallbackMethod = "buildFallbackMethod")
    @Bulkhead(name = "bulkheadHistoricalDataService", type= Type.THREADPOOL, fallbackMethod = "buildFallbackMethod")
    public ResponseEntity<String> postOrderBookData(String data) {
        String historicalDataServiceUrl = "http://historicaldata/orderBooks";
        logger.debug("HistoricalDataRestTemplateClient Correlation id: {}",
                UserContextHolder.getContext().getCorrelationId());
        return restTemplate.postForEntity(historicalDataServiceUrl, data, String.class);
    }

    public ResponseEntity<String> buildFallbackMethod(String data, Exception t) {
        // You can return a default response here in case of a failure
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Request failed due to: " + t.getMessage());
    }
}
