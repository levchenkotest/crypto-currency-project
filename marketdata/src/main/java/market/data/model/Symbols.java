package market.data.model;

import java.util.ArrayList;

public enum Symbols {
    BTCUSDT("BTCUSDT"),
    ETHUSDT("ETHUSDT"),
    XRPUSDT("XRPUSDT"),
    BNBUSDT("BNBUSDT"),
    LTCUSDT("LTCUSDT"),
    ADAUSDT("ADAUSDT"),
    DOTUSDT("DOTUSDT"),
    UNIUSDT("UNIUSDT"),
    DOGEUSDT("DOGEUSDT"),
    LINKUSDT("LINKUSDT");

    private final String symbol;

    Symbols(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public static ArrayList<String> getAllSymbols() {
        ArrayList<String> symbolsList = new ArrayList<>();
        for (Symbols symbol : Symbols.values()) {
            symbolsList.add(symbol.getSymbol());
        }
        return symbolsList;
    }
}
