package historical.data.controller;

import historical.data.client.AnalyticsRestTemplateClient;
import historical.data.model.Ticker;
import historical.data.service.TickerService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class TickerController {

    private final TickerService tickerService;
    private final AnalyticsRestTemplateClient restTemplateClient;

    @PostMapping("/tickers")
    public ResponseEntity<String> createTickers(@RequestBody String jsonString) {
        try {
            List<Ticker> tickers = tickerService.parseAndSaveTickers(jsonString);
            ResponseEntity<String> response = restTemplateClient.sendTickersToAnalytics(tickers);

            if (response.getStatusCode().is2xxSuccessful()) {
                return ResponseEntity.ok("Price alerts send successfully.");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to store historical price.");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error creating tickers: " + e.getMessage());
        }
    }
}
