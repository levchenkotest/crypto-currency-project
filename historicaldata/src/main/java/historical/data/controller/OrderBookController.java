package historical.data.controller;

import historical.data.client.AnalyticsRestTemplateClient;
import historical.data.model.OrderBook;
import historical.data.service.OrderBookService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class OrderBookController {

    private final OrderBookService orderBookService;
    private final AnalyticsRestTemplateClient restTemplateClient;

    @PostMapping("/orderBooks")
    public ResponseEntity<String> createOrderBooks(@RequestBody String jsonString) {
        try {
            List<OrderBook> orderBooks = orderBookService.parseAndSaveOrderBooks(jsonString);
            ResponseEntity<String> response = restTemplateClient.sendOrderBooksToAnalytics(orderBooks);

            if (response.getStatusCode().is2xxSuccessful()) {
                return ResponseEntity.ok("Price alerts send successfully.");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Failed to store historical price.");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error creating orderBooks: " + e.getMessage());
        }
    }
}
