package historical.data.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import historical.data.model.Ticker;
import historical.data.repository.TickerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class TickerService {

    private final TickerRepository tickerRepository;
    private final ObjectMapper objectMapper;

    public List<Ticker> parseAndSaveTickers(String jsonString) throws JsonProcessingException {
        List<Ticker> tickers = objectMapper.readValue(jsonString, objectMapper.getTypeFactory()
                .constructCollectionType(List.class, Ticker.class));
        tickerRepository.saveAll(tickers);
        return tickers;
    }
}
