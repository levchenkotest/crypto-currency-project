package historical.data.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import historical.data.model.OrderBook;
import historical.data.repository.OrderBookRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class OrderBookService {

    private final OrderBookRepository repository;
    private final ObjectMapper objectMapper;

    public List<OrderBook> parseAndSaveOrderBooks(String jsonString) throws JsonProcessingException {
        List<OrderBook> orderBooks = objectMapper.readValue(jsonString, objectMapper.getTypeFactory()
                .constructCollectionType(List.class, OrderBook.class));
        repository.saveAll(orderBooks);
        return orderBooks;
    }
}
