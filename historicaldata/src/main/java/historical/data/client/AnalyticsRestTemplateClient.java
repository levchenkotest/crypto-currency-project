package historical.data.client;

import historical.data.model.OrderBook;
import historical.data.model.Ticker;
import io.github.resilience4j.bulkhead.annotation.Bulkhead;
import io.github.resilience4j.bulkhead.annotation.Bulkhead.Type;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
@AllArgsConstructor
public class AnalyticsRestTemplateClient {
    private final RestTemplate restTemplate;

    @CircuitBreaker(name = "analyticsService", fallbackMethod = "buildFallbackMethod")
    @RateLimiter(name = "analyticsService", fallbackMethod = "buildFallbackMethod")
    @Retry(name = "retryAnalyticsService", fallbackMethod = "buildFallbackMethod")
    @Bulkhead(name = "bulkheadAnalyticsService", type= Type.THREADPOOL, fallbackMethod = "buildFallbackMethod")
    public ResponseEntity<String> sendOrderBooksToAnalytics(List<OrderBook> orderBooks) {
        String analyticDataSrvUrl = "http://analytics/orderBook-analytics";
        return restTemplate.postForEntity(analyticDataSrvUrl, orderBooks, String.class);
    }

    @CircuitBreaker(name = "analyticsService", fallbackMethod = "buildFallbackMethod")
    @RateLimiter(name = "analyticsService", fallbackMethod = "buildFallbackMethod")
    @Retry(name = "retryAnalyticsService", fallbackMethod = "buildFallbackMethod")
    @Bulkhead(name = "bulkheadAnalyticsService", type= Type.THREADPOOL, fallbackMethod = "buildFallbackMethod")
    public ResponseEntity<String> sendTickersToAnalytics(List<Ticker> tickers) {
        String analyticDataSrvUrl = "http://analytics/price-analytics";
        return restTemplate.postForEntity(analyticDataSrvUrl, tickers, String.class);
    }

    public ResponseEntity<String> buildFallbackMethod(List<?> items, Exception t) {
        // You can return a default response here in case of a failure
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Request failed due to: " + t.getMessage());
    }
}
