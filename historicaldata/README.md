# Historical Data Service

Historical Data Service: Stores and retrieves historical price data. It exposes REST API endpoints to store and retrieve historical prices based on various criteria, such as cryptocurrency symbol and time range.

## Requirements

- Java 17
- Maven
- Docker

## Getting Started

### Building the Project

From the project root directory, run:

```bash
mvn clean package

## Before running the Dockerfile, you need to create a jar file from your Spring Boot application

1. Navigate to the root directory of your Spring Boot application.

2. Run the Maven package command to generate the jar file of your application:
mvn clean package

3. Build your Docker image using the Docker build command. Replace target/historicaldata-0.0.1-SNAPSHOT.jar with the path to your actual jar file:
docker build -t historicaldata-service --build-arg JAR_FILE=target/historicaldata-0.0.1-SNAPSHOT.jar .

This command tells Docker to build an image using the Dockerfile in the current directory (indicated by the . at the end) and tag it (-t) as "historicaldata-service". The --build-arg option specifies the jar file to be copied into the image. Note that . refers to the context or location of files that Docker will use for the build.

4. After the image is built, you can run the Docker container using the Docker run command:
docker run -p 8081:8081 historicaldata-service